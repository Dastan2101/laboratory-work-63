import React, {Component, Fragment} from 'react';
import Modal from "../../components/UI/Modal/Modal";
import Spinner from "../../components/UI/Spinner";

const withErrorHandler = (WrappedComponent, axios) => {
    return class WithErrorHOC extends Component {

        constructor(props) {
            super(props);

            this.state = {
                error: null,
            };

            axios.interceptors.response.use(res => res, error => {
                this.setState({error});
                throw error
            });
        }

        errorDismissed = () => {
            this.setState({error: null})
        };

        render() {
            // let wrappedComponent = <WrappedComponent {...this.props} />;

            // if (this.state.loading) {
            //     wrappedComponent = <Spinner/>
            // }
            return (
                <Fragment>

                    <Modal show={this.state.error} close={this.errorDismissed}>
                        {this.state.error && this.state.error.message}
                    </Modal>
                    {/*{wrappedComponent}*/}
                    <WrappedComponent {...this.props} />
                </Fragment>

        )
        }

    };
};

export default withErrorHandler;