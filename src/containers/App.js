import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import HomePage from "../components/HomePage/HomePage";
import AddPosts from "../components/AddPosts/AddPosts";
import ChangePost from "../components/ChangePost/ChangePost";
import Post from "../components/Post/Post";
import About from '../components/About/About'
import Contacts from '../components/Contacts/Contacts'

class App extends Component {
    render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={HomePage}/>
                        <Route path="/add" component={AddPosts}/>
                        <Route path="/posts/:id/edit" component={ChangePost}/>
                        <Route path="/posts/:id" component={Post}/>
                        <Route path="/about" component={About}/>
                        <Route path="/contacts" component={Contacts}/>
                    </Switch>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
