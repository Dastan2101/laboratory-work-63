import React, {Component, Fragment} from 'react';
import Header from "../Header/Header";
import axios from "../../containers/axios-posts";
import {Container, CardTitle, Card, Button} from 'reactstrap';
import './HomePage.css';
import withErrorHandler from "../../hoc/WithErrorHandler/WithErrorHandler";
import Spinner from "../UI/Spinner";


class HomePage extends Component {

    state = {
        posts: null,
        loading: true
    };

    componentDidMount() {
            axios.get('posts.json').then(response => {
                const posts = Object.keys(response.data).map(id => {
                    return {...response.data[id], id}
                });

                this.setState({posts, loading: false});

            }).catch(error => {
                return error;
            });

    };

    getPost = (index) => {
        const copyState = this.state.posts;
        const getIndex = {...copyState[index]};
        this.props.history.push('/posts/' + getIndex.id + '.json');

    };


    render() {
        let posts = null;

        if (this.state.loading) {
            posts = <Spinner/>
        }
        if (this.state.posts) {
            posts = this.state.posts.map((post, key) => (
                    <div className="posts-block" key={key} style={{width: '300px', marginTop: '10px'}}>
                        <Card body>
                            <CardTitle>{post.post.title}</CardTitle>
                            <Button color="secondary" onClick={() => this.getPost(key)}>Read More >></Button>
                        </Card>
                    </div>
                )
            )
        }
        return (
            <Fragment>
                <Header/>
                <Container>
                    {posts}
                </Container>
            </Fragment>
        );
    }
}

export default withErrorHandler(HomePage, axios);