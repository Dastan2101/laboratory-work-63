import React, {Component, Fragment} from 'react';
import './AddPosts.css';
import axios from '../../containers/axios-posts';
import Header from "../Header/Header";

class AddPosts extends Component {

    state = {
        title: '',
        description: ''
    };

    valueChanged = event => {
        const title = event.target.name;
        this.setState({[title]: event.target.value})
    };

    postHandler = (event) => {

        event.preventDefault();

        this.setState({loading: true});

        const posts = {

            post: {
                title: this.state.title,
                description: this.state.description,
            }
        };

        axios.post('/posts.json', posts).finally(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <Header/>
                <div className="form-block">
                    <h2>Add new post</h2>
                    <form onSubmit={this.postHandler} className="form">
                        <div className="title-from">
                            <label style={{display: 'block'}}>Title</label>
                            <input type="text" value={this.state.title} name="title" onChange={this.valueChanged}
                                   className="title"/>
                        </div>
                        <div className="description-from">
                            <label style={{display: 'block'}}>Description</label>
                            <input type="text" value={this.state.description} name="description"
                                   onChange={this.valueChanged} className="description"/>
                        </div>
                        <button type="submit" className="send-btn">Send</button>
                    </form>

                </div>
            </Fragment>
        );
    }
}

export default AddPosts;