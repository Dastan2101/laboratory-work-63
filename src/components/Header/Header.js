import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = () => {
    return (
            <div className="nav-div">
                <div className="container">
                    <ul className="main-nav">
                        <NavLink className='nav-link' to='/'>Home</NavLink>
                        <NavLink className='nav-link' to='/add'>Add</NavLink>
                        <NavLink className='nav-link' to='/about'>About</NavLink>
                        <NavLink className='nav-link' to='/contacts'>Contacts</NavLink>
                    </ul>
                </div>

            </div>
    );
};

export default Header;