import React, {Component} from 'react';
import {Input, Button} from "reactstrap";
import './ChangePost.css';
import axios from "../../containers/axios-posts";
import Spinner from "../UI/Spinner";

class ChangePost extends Component {

    state = {
        title: false,
        description: false,
        loading: false,
    };

    componentDidMount() {
        axios.get('posts/' + this.props.match.params.id).then(response => {
            this.setState({title: response.data.post.title, description: response.data.post.description})
        });

    }

    changeHandler = event => {
        const title = event.target.name;
        this.setState({[title]: event.target.value})
    };

    saveChanged = (event) => {
        event.preventDefault();

        this.setState({loading: true});

        let post = {
            title: this.state.title,
            description: this.state.description
        };

        axios.put('posts/' + this.props.match.params.id, {post: post}).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/')
        })

    };


    render() {
        let form = (
            <form onSubmit={this.saveChanged}>
                <Input value={this.state.title} onChange={this.changeHandler} name="title"/>
                <Input value={this.state.description} onChange={this.changeHandler} name="description"/>
                <Button color="info">Save</Button>
            </form>
        );

        if (this.state.loading) {
            form = <Spinner/>
        }

        return (
            <div className="change-post">
                {form}
            </div>
        );
    }
}

export default ChangePost;