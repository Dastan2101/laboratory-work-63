import React from 'react';
import './About.css'
import Header from "../Header/Header";

const About = () => {
    return (
        <div className='about'>
            <Header/>
            <div className="container">
                <h1 className='about-title'>About us</h1>
                <p className='text'>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Aliquam aliquid eius esse hic molestiae praesentium, quos similique sit suscipit vero?
                </p>
                <p className='text'>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum dolor, fugit harum impedit
                    laborum nam nesciunt obcaecati officia, possimus provident sequi suscipit. Aperiam dolore esse,
                    molestias reiciendis sit temporibus voluptate! At deserunt soluta veritatis.
                    A accusamus beatae consectetur, delectus error, facilis hic iste maxime nobis quae tempora totam voluptatibus!
                </p>
            </div>
        </div>
    );
};

export default About;