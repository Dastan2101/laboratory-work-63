import React, {Component} from 'react';
import axios from '../../containers/axios-posts'
import { Button, FormGroup } from 'reactstrap';


class Post extends Component {

    state = {

        postEdit : {
            title: '',
            description: '',
        }

    };


    componentDidMount() {
        axios.get('posts/' + this.props.match.params.id).then(response => {
            this.setState({postEdit: response.data.post})
        });
    };


    changedPost = () => {

        this.props.history.replace('/posts/' + this.props.match.params.id + '/edit');

    };

    deletePost = () => {

        axios.delete('posts/' + this.props.match.params.id).then(() => {
            this.props.history.replace('/');

        });

    };


    render() {
        return (
            <div style={{width: '500px', margin: '20px auto', border: '1px solid grey', padding: '20px'}}>
                <FormGroup>
                    <h1>{this.state.postEdit.title}</h1>
                    <h1>{this.state.postEdit.description}</h1>
                </FormGroup>
                <div>
                    <Button color="success" onClick={this.changedPost} style={{marginRight: '20px'}}>Edit</Button>
                    <Button color="danger" onClick={this.deletePost}>Delete</Button>

                </div>
            </div>
        );
    }
}

export default Post;